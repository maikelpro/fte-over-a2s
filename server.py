#!/usr/bin/python
# Example on how to use Format Transforming Encryption on the A2S protcol which is used by the SourceEngine
#
# Created by Maikel Zweerink, Oct 2017

import socket
import fte.encoder
import struct
import binascii
import shared
import select
import wikipedia
import sys
import os
# For CTR AES bug in paramiko
import warnings
warnings.filterwarnings("ignore")

# Gameserver to use to rely all legitimate calls to (could just as well be hosted on localhost)
gameserver_address = ('67.213.222.177', 27015)
# Address for our fteserver to bind on
address = ('127.0.0.1', 50007)

def getGatewayWelcome():
	return	"="* 30 + "\n" + \
			"Welcome to the Hidden Gateway\n" + \
			"="* 30 + "\n" + \
			"Send an wikipedia article ID to this gateway, and I will provide the page to you."

# Retrieve a wikipedia article and provide a summary
def getWikiArticle(articleId):
	try:
		article = wikipedia.page(articleId)
		return "{} ({})\n=======\n{}...".format(article.title, article.url, article.summary.encode("utf-8")[:128])
	except:
		print(sys.exc_info()[0])
		return "Server could not retrieve wikipedia page."

# Main program
def main(args):
	# Setup listening socket
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.bind(address)

	# Setup socket to the normal gameserver, to pass all normal requests
	gss = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	socks = [s, gss]
	client_addr = None

	# Generate a random challenge for the client to use (according to the formal A2S spec for rules/players)
	challenge_val = os.urandom(4)
	while 1:
		ready_socks,_,_ = select.select(socks, [], []) 
		for sock in ready_socks:
			data, addr = sock.recvfrom(1024)
			print('Received {}'.format(binascii.hexlify(data)))
			# Validate if the client is requesting a RULES response.
			if sock == s and ord(data[4:5]) == 0x56:
				# The Rules request we will intercept and transfer data over!

				# Challenge request (which mimics the following A2S request):
				# Retreive the server's game mode configuration

				# This method allows you capture a subset of a server's console
				# variables (often referred to as 'cvars',) specifically those which
				# have the ``FCVAR_NOTIFY`` flag set on them. These cvars are used to
				# indicate game mode's configuration, such as the gravity setting for
				# the map or whether friendly fire is enabled or not.

				# The following fields are available on the response:

				# +--------------------+------------------------------------------------+
				# | Field              | Description                                    |
				# +====================+================================================+
				# | response_type      | Always ``0x56``                                |
				# +--------------------+------------------------------------------------+
				# | rule_count         | The number of rules                            |
				# +--------------------+------------------------------------------------+
				# | rules              | A dictionary mapping rule names to their       |
				# |                    | corresponding string value                     |
				# +--------------------+------------------------------------------------+
				challenge = struct.unpack('=i', data[5:9])[0]
				if challenge == -1:
					challenge_res = struct.pack('=Ib', 0xFFFFFFFF, 0x41) + challenge_val
					print('Sending  {}'.format(binascii.hexlify(challenge_res)))
					s.sendto(challenge_res, addr)
				elif challenge == struct.unpack('=i', challenge_val)[0]:
					print('Preparing to send tunneled payload.')
					rules_count = 1 # placeholder for the A2S protocol
					fteSender = fte.encoder.DfaEncoder(shared.server_dfa, shared.fixed_slice_server, K1=shared.K1, K2=shared.K2)
					fteReceiver = fte.encoder.DfaEncoder(shared.client_dfa, shared.fixed_slice_client, K1=shared.K1, K2=shared.K2)
					# Process any queryword from the client to request an wikipedia article
					client_query = '\x00'
					try:
						client_query = str(data[9:])
					except:
						pass
					ciphertext = ''
					if client_query != '\x00' and client_query != '':
						[client_query_plaintext, remainder] = fteReceiver.decode(client_query)
						print('Client asked for wikipedia article {}'.format(binascii.hexlify(client_query_plaintext)))
						ciphertext = fteSender.encode(getWikiArticle(client_query_plaintext))
					else:
						ciphertext = fteSender.encode(getGatewayWelcome())
					hidden_payload =  ciphertext + '\x00'
					
					# set rules_count to satisfy the protocol
					rules_count = int(binascii.hexlify(ciphertext).count('00') / 2)
					print('payload split in {} rules'.format(rules_count))

					[output_plaintext, remainder] = fteSender.decode(hidden_payload)

					print(output_plaintext)

					fte_rules_res = struct.pack('=Ibb', 0xFFFFFFFF, 0x45, int(rules_count)) + hidden_payload
					# return payload
					print('Sending  {}'.format(binascii.hexlify(fte_rules_res)))
					s.sendto(fte_rules_res, addr)
			else:
				# This data is not requesting the RULES, so lets pass it back an forth between the client and gameserver.
				if sock == s:
					# Pass through to gameserver
					print('Sending to gameserver {}'.format(binascii.hexlify(data)))
					client_addr = addr
					gss.sendto(data, gameserver_address)
				else:
					# Pass back to the client
					if client_addr is not None:
						print('Sending back to client {}'.format(binascii.hexlify(data)))
						s.sendto(data, client_addr)	
	s.close()


if __name__ == '__main__':
	main(sys.argv)
