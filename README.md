Using Format-Transforming Encryption (FTE) over A2S SourceEngine queries
======

![](https://maikel.pro/images/fte_over_a2s/header.png)

This is a example on how FTE can be applied to known protocols to hide encrypted data in everyday protocols. This repo contains both a **server** and a **client** which can communicate with eachother over A2S_RULES[2] without being a obviously encrypted datastream. FTE makes use of a Deterministic Finite Automaton (DFA) to unrank/rank encrypted data base off a regular expression [1]. We use the A2S server queries as a "normal" datastream to hide our special gateway data in. This can be helpfull on networks where Deep Packet Inspection (DPI) is being applied to apply censorship or limit the networks capabilities.

With this principle, we can transfer information over a hidden gateway without making it clearly obvious we want to encrypt our information exchange. Similar to stenography, FTE allows us to use our hidden gateway just like a normal TF2 gameserver! You can query the gameserver for information and it responds correctly, just like a normal gameserver should.

[Read the blog post](https://maikel.pro/blog/using-team-fortress-2-game-servers-to-circumvent-censorship) in case you're interested on what's this about.

Installation
----------

List of prerequisites (tested on Ubuntu/Debian Wheezy or higher):

* `Python 2.7` to run the example.
* [libfte](https://github.com/kpdyer/libfte) installed and setup (download repository and run the `setup.py`).
* `pip install python-valve` for this example.
* `pip install wikipedia` for this example.

*I did not test this example from a clean system, updates on what is missing in this guide is appreciated!*


Run the example
----------

* Run `python server.py` to setup our hidden gateway.
* Open another terminal and run `python client.py` which acts as the client.
* If everything went OK you now see a message that welcomes you to the hidden gateway.
* Run `python client.py Self-Censorship` to query Wikipedia from your hidden gateway.
* You retrieve a summary of the Wikipedia's article which was transfered from your gameserver to the client via FTE.

So this all looks nice, but what is actually being sent in the background? First of all; all messages look like the A2S[2] protocol used by SourceEngine gameservers. To further inspect this we will use a SourceEngine gameserver querier which was ment for gameserver info retrieval:

* Goto `cd tools`
* Execute `python query_gameserver.py` and look at the retrieved results.

As a result you get both a list of **players** and **rules** of the gameserver. Inspecting the values by hand, the **players** list looks pretty geniune, because it is. This information is being forwarded to the real gameserver, however, if a client request the **rules list (A2S_RULES)** you trigger the hidden gateway. Inspecting this information by hand, for a novice this information still looks very legitimate. **But actually, our hidden gateway response is in the rules response!** Applying DPI or filtering techniques to FTE would be very hard, as all these packets conform the A2S standard and can almost not be distingquised from allowed packets.



**This project is an example, do not use this in production. Consider using [fte-proxy](https://fteproxy.org/) when you want to use FTE in practice.**

References
----------

Built with the help of: http://docs.python.org/2/library/socket.html, https://github.com/kpdyer/libfte, https://github.com/serverstf/python-valve

[1] [Protocol Misidentification Made Easy with Format-Transforming Encryption](https://kpdyer.com/publications/ccs2013-fte.pdf)
    Kevin P. Dyer, Scott E. Coull, Thomas Ristenpart and Thomas Shrimpton 

[2] [Valve Developer Community - Server queries](https://developer.valvesoftware.com/wiki/Server_queries) Valve