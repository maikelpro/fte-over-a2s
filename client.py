#!/usr/bin/python
# Example on how to use Format Transforming Encryption on the A2S protcol which is used by the SourceEngine
#
# Created by Maikel Zweerink, Oct 2017

import socket
import sys
import fte.encoder
import shared
import valve.source.a2s
# For CTR AES bug in paramiko
import warnings
warnings.filterwarnings("ignore")

# Config
fteserver = ('127.0.0.1', 50007)

# Main program
def main(args):
	server = valve.source.a2s.ServerQuerier(fteserver, timeout=8.0)

	# Commands to execute
	# > python client.py --query-players
	# Queries for the server playerlist according to the A2S protocol (just like normal Valve Gameservers)
	# > python client.py --show-rules
	# Shows the rulelist according to the A2S protocol (just like normal Valve Gameservers), however these are being used by FTE, so it's actually the ciphertext!
	# > python client.py 
	# Connect to the fteserver via the ruelist A2S protocol and retrieve the encrypted ciphertext and decrypt it. Show the result on screen.

	if(len(args) > 1 and args[1] == '--query-players'):
		players = server.players()
		for player in players["players"]:
			print("name={}, score={}, duration={}".format(player["name"].encode('ascii', 'ignore'), player["score"], player["duration"]))
	elif(len(args) > 1 and args[1] == '--show-rules'):
		rules = server.rules()
		for rule in rules["rules"]:
			print("{} : {}".format(str(rule), rules["rules"][rule].encode('ascii', 'ignore')))
	else:
		query = ""
		if(len(args) > 1):
			query = args[1]
		fteReceiver = fte.encoder.DfaEncoder(shared.server_dfa, shared.fixed_slice_server, K1=shared.K1, K2=shared.K2)
		fteSender = fte.encoder.DfaEncoder(shared.client_dfa, shared.fixed_slice_client, K1=shared.K1, K2=shared.K2)
		tunnel_res = server.tunnel(fteReceiver, fteSender, request_wiki_article=query)
		print(tunnel_res)
		

if __name__ == '__main__':
	main(sys.argv)
