#!/usr/bin/python
import valve.source.a2s

addr = ('127.0.0.1', 50007)

server = valve.source.a2s.ServerQuerier(addr, timeout=8.0)
players = server.players()
for player in players["players"]:
	print("name={}, score={}, duration={}".format(player["name"].encode('ascii', 'ignore'), player["score"], player["duration"]))
rules = server.rules()
for rule in rules["rules"]:
	print("{} : {}".format(str(rule), rules["rules"][rule].encode('ascii', 'ignore')))
