#!/usr/bin/python
import fte.encoder
import binascii

# Text we want to encrypt
plaintext = "Hello open internet!"

key = '\xff'*16

# ^(a|b|c|d|e|f)+$
# This is the regular expression converted to a DFA
dfa = "0	1	97	97 \n" + \
"0	1	98	98 \n" + \
"0	1	99	99 \n" + \
"0	1	100	100 \n" + \
"0	1	101	101 \n" + \
"0	1	102	102 \n" + \
"1	1	97	97 \n" + \
"1	1	98	98 \n" + \
"1	1	99	99 \n" + \
"1	1	100	100 \n" + \
"1	1	101	101 \n" + \
"1	1	102	102 \n" + \
"1"


fteObj = fte.encoder.DfaEncoder(dfa, 256, K1=key, K2=key)
ciphertext = fteObj.encode(plaintext)


print('Plaintext  {}'.format(binascii.hexlify(plaintext)))
print('Ciphertext  {}'.format(binascii.hexlify(ciphertext)))