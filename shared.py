#!/usr/bin/python
# Example on how to use Format Transforming Encryption on the A2S protcol which is used by the SourceEngine
#
# Created by Maikel Zweerink, Oct 2017

fixed_slice_server = 1024
fixed_slice_client = 128
K1 = '\x01\x1c\xff\x05\xff\xff\xff\x0d\xff\xff\xff\xff\xab\xff\xa4\xb3'
K2 = '\x0f\x06\x00\xff\xff\xff\xff\x5f\x84\xff\xff\xff\xff\x3f\xff\x9c'


#from dfa import tf2_rules_easy as dfa_server_import
#from dfa import tf2_rules_expert as dfa_server_import
#from dfa import tf2_rules_expert_scrambled as dfa_server_import
from dfa import tf2_rules_expert_stealth as dfa_server_import
from dfa import tf2_rules_easy as dfa_client_import

server_dfa = dfa_server_import.rules_dfa
client_dfa = dfa_client_import.rules_dfa
